package com.agiletestingalliance.controller;

import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.agiletestingalliance.Minimum;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class MinimumTest {

      Minimum myCalc = new Minimum();
	
	@Test	
      public void testCalc1() {

      int actual = myCalc.find(3,5);
      assertEquals("Answer", 3, actual);
	
      }

     
	@Test	
      public void testCalc2() {

      int actual = myCalc.find(6,2);
      assertEquals("Answer", 2, actual);
	
      }

}
