package com.agiletestingalliance.controller;

import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import com.agiletestingalliance.AboutCPDOF;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class AboutCPDOFTest {

      AboutCPDOF var = new AboutCPDOF();
	
	@Test	
      public void testAboutCPDOF() {

      String actual = var.desc();
      assertTrue(actual.contains("end to end DevOps Life Cycle"));
	
      }

     

}
