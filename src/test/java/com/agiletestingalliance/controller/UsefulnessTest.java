package com.agiletestingalliance.controller;

import org.junit.*;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import com.agiletestingalliance.Usefulness;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class UsefulnessTest {

      Usefulness var = new Usefulness();
	
	@Test	
      public void testUsefulness() {

      String actual = var.desc();
      assertTrue(actual.contains("building quality in, improving productivity"));
	
      }

     

}
